$(function() {

	var captcha = $('#botdetect-captcha').captcha({
		captchaEndpoint : '/botdetectcaptcha'
	});
	


	$("#submitBtn").on(
			"click",
			function(e) {
				e.preventDefault();
				var captchaId = captcha.getCaptchaId();
				var captchaCode = $('#captchaCode').val();
				var username = $('#username').val();
				var password = $('#password').val();
				proccessLogin(makeLoginVoObject(captchaId, captchaCode,
						username, password));
			});

	function makeLoginVoObject(captchaId, captchaCode, username, password) {
		var loginVO = new Object();
		var captchaVO = new Object();

		captchaVO.captchaId = captchaId;
		captchaVO.captchaCode = captchaCode;

		loginVO.username = username;
		loginVO.password = password;
		loginVO.captchaVO = captchaVO;
		
		return loginVO;
	}

	function proccessLogin(loginVO) {
		$.ajax({
			method : 'POST',
			url : './captcha/validate',
			contentType : "application/json",
			data : JSON.stringify(loginVO),
			success : function(response) {
				
			},
			complete : function(response) {
				if(response.status==200){
					localStorage.setItem("token", response.responseText);
					window.location = "./sidebar";
				}else{
					alert(response.responseText);
				}
				captcha.reloadImage();
			}
		});
	}

});