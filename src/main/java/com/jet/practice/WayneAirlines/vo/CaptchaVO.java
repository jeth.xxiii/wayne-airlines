package com.jet.practice.WayneAirlines.vo;

public class CaptchaVO {

	private String captchaId;
	private String captchaCode;

	public String getCaptchaId() {
		return captchaId;
	}

	public void setCaptchaId(String captchaId) {
		this.captchaId = captchaId;
	}

	public String getCaptchaCode() {
		return captchaCode;
	}

	public void setCaptchaCode(String captchaCode) {
		this.captchaCode = captchaCode;
	}

	@Override
	public String toString() {
		return "CaptchaObject [captchaId=" + captchaId + ", captchaCode=" + captchaCode + "]";
	}

}
