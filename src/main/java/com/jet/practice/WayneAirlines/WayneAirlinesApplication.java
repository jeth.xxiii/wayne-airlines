package com.jet.practice.WayneAirlines;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WayneAirlinesApplication {

	public static void main(String[] args) {
		SpringApplication.run(WayneAirlinesApplication.class, args);
	}
}
