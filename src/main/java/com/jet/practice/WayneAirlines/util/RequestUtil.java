package com.jet.practice.WayneAirlines.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.jet.practice.WayneAirlines.vo.LoginAuthVO;

public class RequestUtil {
	
	public static String postToAuthToken(LoginAuthVO authVO) throws HttpClientErrorException {
		RestTemplate restTemplate = new RestTemplate();
		String uri = "http://localhost:6971/auth";
		List<String> token = new ArrayList<>();
		try {
			ResponseEntity<String> response = restTemplate.postForEntity(uri, authVO, String.class);
			token = response.getHeaders().get("Authorization");
		} catch (HttpClientErrorException hce) {
			System.out.println(hce.getMessage());
			return HttpStatus.UNAUTHORIZED.toString();
		}

		return token.get(0);

	}

	
}
